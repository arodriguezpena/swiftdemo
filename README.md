# FIFClipboard

[![CI Status](https://img.shields.io/travis/arodriguezp2003/FIFClipboard.svg?style=flat)](https://travis-ci.org/arodriguezp2003/FIFClipboard)
[![Version](https://img.shields.io/cocoapods/v/FIFClipboard.svg?style=flat)](https://cocoapods.org/pods/FIFClipboard)
[![License](https://img.shields.io/cocoapods/l/FIFClipboard.svg?style=flat)](https://cocoapods.org/pods/FIFClipboard)
[![Platform](https://img.shields.io/cocoapods/p/FIFClipboard.svg?style=flat)](https://cocoapods.org/pods/FIFClipboard)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

FIFClipboard is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'FIFClipboard'
```

## Author

arodriguezp2003, alejandrorodriguezpena@gmail.com

## License

FIFClipboard is available under the MIT license. See the LICENSE file for more info.

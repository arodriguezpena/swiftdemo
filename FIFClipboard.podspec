#
# Be sure to run `pod lib lint FIFClipboard.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'FIFClipboard'
  s.version          = '0.1.5'
  s.summary          = 'Copia del portapapeles los datos bancarios de un contacto'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://bitbucket.org/arodrigueztechnisys/fifclipboard-ios'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'arodriguezp2003' => 'alejandrorodriguezpena@gmail.com' }
  s.source           = { :git => 'https://bitbucket.org/arodrigueztechnisys/fifclipboard-ios.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'
  
  s.static_framework = true
  
  s.source_files = 'FIFClipboard/Classes/**/*'
  s.resources = 'FIFClipboard/Assets/**/*.{ttf,otf,storyboard,xib,xcassets,json,imageset,png,html}'
  s.test_spec 'FIFClipboard_Tests' do |test_spec|
      test_spec.source_files = 'FIFClipboard/Test/*.swift'
  end
  
  # s.resource_bundles = {
  #   'FIFClipboard' => ['FIFClipboard/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end

//
//  WireTransferViewController.swift
//  FIFClipboard_Example
//
//  Created by Alejandro  Rodriguez on 7/8/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import FIFClipboard
class WireTransferViewController: UIViewController, FIFClipboardPresenterProtocol {

  
    
    var vc: FIFClipboardViewController?
  
    override func viewDidLoad() {
        super.viewDidLoad()
      
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
         vc = FIFClipboardFactory().getFIFClipboardViewController(currentView: self, banks: [])
        vc!.setDelegate(self)
    }
    
    @IBAction func closeTap(_ sender: Any) {
        self.dismiss(animated: true)
        self.vc!.destroy() 
    }
    

    
    func getData(rut: String, name: String, email: String, type: String, typeIndex: Int, number: String, bankIndex: Int, bankName: String, bankMnemonic: String) {
        
    }
    
}

//
//  ModelUnitTesting.swift
//  Pods-FIFClipboard_Tests
//
//  Created by Alejandro  Rodriguez on 7/8/19.
//

import XCTest
@testable import FIFClipboard
class ModelUnitTesting: XCTestCase {

    
   
    
    override func setUp() {
    }

    override func tearDown() {}

    
    func test_1() {
        let model = FIFClipboardModel(banks: getBank())
        
        
        let str = """
Alejandro Rodriguez
16327533-3
Banco Bci
Cuenta ahorro
9374674646
alejandrorodriguezpena@gmail.com
"""
        
        
        let account = model.checkClipboard(At: str)
        
       XCTAssert(account != nil)
    }
    func test_ahorro() {
        let model = FIFClipboardModel(banks: getBank())
        
        
        let str = """
Alejandro Rodriguez
16327533-3
Banco Bci
Cuenta de ahorro
9374674646
alejandrorodriguezpena@gmail.com
"""
        
        
        let account = model.checkClipboard(At: str)
        
        XCTAssert(account != nil)
    }
    
    
    func test_2() {
        let model = FIFClipboardModel(banks: getBank())
        
        
        let str = """
Alejandro Rodriguez
alejandrorodriguezpena@gmail.com
Banco Itau
9374674646
Cuenta vista
16327533-3
"""
        
        
        let account = model.checkClipboard(At: str)
    
        
        XCTAssert(account != nil)
    }
    
    
    func test_wRUT() {
        let model = FIFClipboardModel(banks: getBank())
        
        
        let str = """
Alejandro Rodriguez
alejandrorodriguezpena@gmail.com
Banco Itau
9374674646
Cuenta Corriente
"""
        
        
        let account = model.checkClipboard(At: str)
        
        XCTAssert(account == nil)

        
     
    }

    
    func test_wEmail() {
        let model = FIFClipboardModel(banks: getBank())
        
        
        let str = """
Alejandro Rodriguez
Banco Itau
9374674646
Cuenta Corriente
16327533-3
"""
        
        
        let account = model.checkClipboard(At: str)
        
        XCTAssert(account == nil)

    }
    func test_wBankName() {
        let model = FIFClipboardModel(banks: getBank())
        
        
        let str = """
Alejandro Rodriguez
alejandrorodriguezpena@gmail.com
9374674646
Cuenta Corriente
16327533-3
"""
        
        
        let account = model.checkClipboard(At: str)
        
        XCTAssert(account == nil)

       
    }
    func test_wTypeAccount() {
        let model = FIFClipboardModel(banks: getBank())
        
        
        let str = """
Alejandro Rodriguez
16327533-3
Banco Itau
9374674646
alejandrorodriguezpena@gmail.com
"""
        
        
        let account = model.checkClipboard(At: str)
        
        XCTAssert(account == nil)

        
        
    }
    
    func test_wLimitLine() {
        let model = FIFClipboardModel(banks: getBank())
        
        
        let str = """
Banco Itau
9374674646
Cuenta Corriente
"""
        
        
        let account = model.checkClipboard(At: str)
        
        XCTAssert(account == nil)

    }
    
    func test_NoNumber() {
        let model = FIFClipboardModel(banks: getBank())
        
        
        let str = """
Alejandro Rodriguez
alejandrorodriguezpena@gmail.com
Banco bci
Cuenta vista
16327533-3
"""
        
        
        let account = model.checkClipboard(At: str)
        
        XCTAssert(account == nil)
        
        
    }
    
    func test_NoBank() {
        let model = FIFClipboardModel(banks: getBank())
        
        
        let str = """
Alejandro Rodriguez
alejandrorodriguezpena@gmail.com
Banco SSH
9374674646
Cuenta vista
16327533-3
"""
        
        
        let account = model.checkClipboard(At: str)
        
       XCTAssert(account == nil)
        
        
    }
    
    func test_Factory() {
        let vc = FIFClipboardFactory().getFIFClipboardViewController(currentView: UIViewController(), banks: [])
        XCTAssert(vc.presenter != nil)
    }
}


fileprivate extension ModelUnitTesting {
    func getBank() -> NSArray {
        var array: [NSObject] = []
        
        let bx = ["name": "Banco Falabella", "mnemonic": "BANCO_FALABELLA"]
        array.append(bx as NSObject)
        let bxx = ["name": "Banco Itaú", "mnemonic": "BANCO_ITAU"]
        array.append(bxx as NSObject)
        let bxx2 = ["name": "bci otro bk", "mnemonic": "BANCO_BCI"]
        array.append(bxx2 as NSObject)
        return array as NSArray
        
    }
    
}

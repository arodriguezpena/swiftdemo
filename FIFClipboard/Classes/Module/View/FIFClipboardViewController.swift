//
//  ConfirmViewController.swift
//  FIFClipboard
//
//  Created by Alejandro  Rodriguez on 6/17/19.
//

import UIKit


protocol FIFClipboardDisplayLogic: class {
    func setDelegate(_ delegate: FIFClipboardPresenterProtocol) -> Void
    func destroy() -> Void
}

@objc public class FIFClipboardViewController: UIViewController, FIFClipboardDisplayLogic {
   
   
    @objc public func setDelegate(_ delegate: FIFClipboardPresenterProtocol) {
        presenter?.delegate = delegate
        
    }
    
    @objc public func destroy() {
        presenter?.destroy()
    }
    
    var presenter: FIFClipboardPresentationLogic?
    
    @IBOutlet weak var xTitle: UILabel!
    @IBOutlet weak var xName: UILabel!
    @IBOutlet weak var xRut: UILabel!
    @IBOutlet weak var xEmail: UILabel!
    @IBOutlet weak var xBank: UILabel!
    @IBOutlet weak var xTypeAccount: UILabel!
    @IBOutlet weak var xNumber: UILabel!
    @IBOutlet weak var constraints: NSLayoutConstraint!
    @IBOutlet weak var Container: UIView!
    @IBOutlet weak var viewContainer: UIView!
    
    // MARK: Object lifecycle
    init() {
        super.init(nibName: String(describing: FIFClipboardViewController.self),
                   bundle: Bundle(for: FIFClipboardViewController.classForCoder()))
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.modalTransitionStyle = .crossDissolve
        self.modalPresentationStyle = .overCurrentContext
        self.setup()
    }

    func setup() {
        if UIDevice.current.hasNotch  {
            self.constraints.constant = 30
        }
        self.viewContainer.layer.cornerRadius = 12
    
        self.xName.text = presenter?.account?.name
        self.xRut.text =  presenter?.account?.rut
        self.xEmail.text =  presenter?.account?.email
        self.xBank.text = presenter?.account?.bank?.name
        self.xTypeAccount.text = presenter?.account?.type
        self.xNumber.text = presenter?.account?.number


        
        let tapContainer = UITapGestureRecognizer(target: self, action: #selector(self.cancelTap2))
        tapContainer.delegate = self
        self.Container.addGestureRecognizer(tapContainer)
    }
    
    

    @objc func cancelTap2(r:UITapGestureRecognizer) {
        if r.state == .ended  {
            self.dismiss(animated: true)
        }
    }
    
    
    @IBAction func cancelTap(_ sender: Any) {
        self.dismiss(animated: true)
    }
    @IBAction func acceptTap(_ sender: Any) {
        self.dismiss(animated: true) {
            self.presenter?.acceptedButton()
        }
    }
    
}

extension FIFClipboardViewController:UIGestureRecognizerDelegate {
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view?.isDescendant(of: self.viewContainer))!{
            return false
        }
        return true
    }
}

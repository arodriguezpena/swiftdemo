//
//  ext-fifclipboard.swift
//  FIFClipboard
//
//  Created by Alejandro  Rodriguez on 7/7/19.
//

import Foundation
extension String {
    
    func g() -> String {
        if self == "" {
            return "-"
        }
        return self
    }
    func trim() -> String{
        return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
    }
    func format() -> String {
        var _str = self.replacingOccurrences(of: "Nº", with: "").trim()
        _str = _str.replacingOccurrences(of: "-", with: "").trim()
        
        return _str
    }
    
    
    func isEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    func isNumber() -> Bool {
        return !isEmpty && rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
    func isRut() -> Bool {
        return Rut.validadorRut(input: self)
    }
    func isBank() -> Bool {
        return self.lowercased().contains("banco")
    }
    func isType() -> Bool {
        return self.lowercased().contains("cuenta")
    }
}

class Rut {
    static func validadorRut(input: String!) -> Bool{
        var rut = input.replacingOccurrences(of: ".", with: "", options: NSString.CompareOptions.literal, range : nil)
        rut = rut.replacingOccurrences(of: "-", with: "", options: NSString.CompareOptions.literal, range : nil)
        rut = rut.uppercased()
        
        if (rut.count < 8 || rut.count > 10){
            return false
        }
        
        let rutRegex = "^(\\d{1,3}(\\.?\\d{3}){2})\\-?([\\dkK])$"
        
        let rutTest = NSPredicate(format: "SELF MATCHES %@", rutRegex)
        
        if (!rutTest.evaluate(with: rut)) {
            return false
        }
        
        let runTovalidate = getRutDv(value: input)
        
        let rutNumber = runTovalidate.0
        let rutDV = runTovalidate.1
        
        let calculatedDV = validateDV(rut: rutNumber)
        
        if (rutDV != calculatedDV){
            return false
        }
        
        return true
    }
    
    static func getRutDv(value: String) -> (String, String){
        if (value.isEmpty || value.count < 2){
            return ("", "")
        }
        
        var rut = value.replacingOccurrences(of: ".", with: "", options: NSString.CompareOptions.literal, range: nil)
        rut = rut.replacingOccurrences(of: "-", with: "", options: NSString.CompareOptions.literal, range: nil)
        //        var rut = value
        let dv: String = String(rut.last!)
        let run: String = String(rut.dropLast())
        
        return (run, dv)
    }
    
    static func validateDV(rut: String) -> String?{
        var acumulado : Int = 0
        var ti : Int = 2
        
        for index in stride(from: rut.count-1, through: 0, by: -1) {
            let n = Array(rut)[index]
            let nl = String(n)
            guard let vl = Int(nl) else {
                return ""
            }
            
            
            acumulado += vl * ti
            
            if (ti == 7) {
                ti = 1
            }
            ti += 1
        }
        
        let resto : Int = acumulado % 11
        let numericDigit : Int = 11 - resto
        var digito : String = ""
        
        
        if (numericDigit <= 9){
            digito = String(numericDigit);
        }else if (numericDigit == 10){
            digito = "K"
        }else{
            digito = "0"
        }
        
        return digito
    }
}

extension UIDevice {
    var hasNotch: Bool {
        if #available(iOS 11.0, *) {
            let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
            return bottom > 0
        } else {
            return false
        }
        
    }
}
